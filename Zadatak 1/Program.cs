﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note("Note ONE", "Ovo je zabilješka jedan");
            Note note2 = new Note("Note TWO", "Ovo je zabilješka dva");
            Note note3 = new Note("Note THREE", "Ovo je zabilješka tri");

            Notebook notebook = new Notebook();

            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);

            IAbstractIterator iterator = notebook.GetIterator();

            int i;
            for (i=0 ; i<notebook.Count ; i++)
            { 
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
