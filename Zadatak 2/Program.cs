﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product1 = new Product("Product ONE", 9.99);
            Product product2 = new Product("Product TWO", 19.99);
            Product product3 = new Product("Product THREE", 29.99);

            Box box = new Box();

            box.AddProduct(product1);
            box.AddProduct(product2);
            box.AddProduct(product3);

            IAbstractIterator iterator = box.GetIterator();

            int i;
            for (i = 0; i < box.Count; i++)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
            }            
        }
    }
}